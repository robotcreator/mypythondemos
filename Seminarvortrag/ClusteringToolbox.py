import csv
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button, TextBox
from sklearn.cluster import KMeans


class Dataset:

    def __init__(self):
        self.X = []                         # input x coordinate
        self.Y = []                         # input y coordinate
        self.A = []                         # input angle coordinate
        self.dX = []                        # output delta x
        self.dY = []                        # output delta y
        self.dA = []                        # output delta angle
        self.X_N = []                       # input x coordinate of nearest neighbors
        self.Y_N = []                       # input y coordinate of nearest neighbors
        self.dX_N = []                      # output x coordinate of nearest neighbors
        self.dY_N = []                      # output y coordinate of nearest neighbors
        self.number_of_points = 0           # total number of data points

    def read_data(self, input_file, option=1):

        # The training and test data will be saved in these lists
        inputs, outputs = [], []

        # Read training.csv and extract the necessary data
        with open(input_file) as csvfile:
            readTrainingCSV = csv.reader(csvfile, delimiter=';')
            for row in readTrainingCSV:
                if len(row) < 9:
                    continue
                try:
                    inputs.append(np.array([float(row[2]), float(row[3]), float(row[4])]))
                    if option == 1:
                        outputs.append(np.array([float(row[6]), float(row[7]), float(row[8])]))
                    if option == 2:
                        outputs.append(np.array([float(row[5]), float(row[6]), float(row[7])]))

                except:
                    print("Row skipped")

        # Data pre-processing
        self.X, self.Y, self.A, self.dX, self.dY, self.dA = [], [], [], [], [], []

        # Write the data to vectors for each column
        for i in range(0, len(inputs)):
            self.X.append(inputs[i][0])
            self.Y.append(inputs[i][1])
            self.A.append(inputs[i][2])
            self.dX.append(outputs[i][0])
            self.dY.append(outputs[i][1])
            self.dA.append(outputs[i][2])

        self.number_of_points = len(self.X)


class ClusteringToolbox:

    def __init__(self, data):
        # Establish new dataset object and read the data
        self.data = data
        self.number_of_points = 0           # total number of data points
        self.number_of_clusters = 25        # number of clusters
        self.angle_weight = 0               # angle weight for calculation of the neighborhood
        self.clusters = []                  # contains all datapoints grouped by the clusters
        self.K_list = []                    # list of cluster indices
        self.inputs = []
        self.outputs = []
        self.clustering_state = "inputs"

        self.fig, (self.ax1, self.ax2) = plt.subplots(1, 2)
        self.noc_slider = Slider(self.ax1, 'init', 0, 0, valinit=0, valstep=0)
        self.ang_slider = Slider(self.ax2, 'init', 0, 0, valinit=0, valstep=0)

    def calc_inputs_outputs_list(self):
        self.inputs, self.outputs = [], []
        data = self.data

        for i in range(0, data.number_of_points):
            self.inputs.append([data.X[i], data.Y[i], data.A[i] * self.angle_weight])
            self.outputs.append([data.dX[i], data.dY[i], data.dA[i] * self.angle_weight])

    def clustering(self):

        data = self.data
        K = self.number_of_clusters
        kmeans = KMeans(n_clusters=K, random_state=0).fit(eval("self." + self.clustering_state))
        self.K_list = kmeans.labels_
        self.clusters = []
        for j in range(0, K):
            cluster_points_list = []
            for i in range(0, len(self.K_list)):
                if self.K_list[i] == j:
                    cluster_points_list.append([data.X[i], data.Y[i], data.A[i], data.dX[i], data.dY[i], data.dA[i]])
            self.clusters.append(cluster_points_list)

    def create_figure(self):

        self.fig.canvas.set_window_title('Clustering Toolbox')
        self.plot_data()
        self.fig.show()

        # Setup control elements (buttons & sliders)
        ax_color = 'lightblue'
        raw_ax = plt.axes([0.1, 0.08, 0.1, 0.03])
        final_ax = plt.axes([0.22, 0.08, 0.1, 0.03])
        pos3 = plt.axes([0.1, 0.03, 0.3, 0.03], facecolor=ax_color)
        pos4 = plt.axes([0.6, 0.03, 0.3, 0.03], facecolor=ax_color)

        outputs_button = Button(final_ax, 'Outputs', color='lightblue', hovercolor='0.975')
        inputs_button = Button(raw_ax, 'Inputs', color='lightblue', hovercolor='0.975')

        self.noc_slider = Slider(pos3, 'K-means', 0, 100, valinit=25, valstep=1)
        self.ang_slider = Slider(pos4, 'Angle weight', 0, 20, valinit=0, valstep=0.1)

        outputs_button.on_clicked(self.outputs_button_pressed)
        inputs_button.on_clicked(self.inputs_button_pressed)
        self.noc_slider.on_changed(self.updated)
        self.ang_slider.on_changed(self.updated)

        plt.subplots_adjust(left=0.1, right=0.9)
        plt.show()

    def plot_data(self):
        data = self.data
        cmap = plt.cm.get_cmap("gist_rainbow_r", self.number_of_clusters + 1)  # colormap for clustering visualization

        ax1 = self.ax1
        ax1.cla()
        ax1.axhline(y=0, color='gray', linewidth=0.5)
        ax1.axvline(x=0, color='gray', linewidth=0.5)
        ax1.set_aspect('equal')
        ax1.grid(True, which='both', linewidth=0.5, linestyle='--')
        ax1.set_title("Input data")

        ax2 = self.ax2
        ax2.cla()
        ax2.axhline(y=0, color='gray', linewidth=0.5)
        ax2.axvline(x=0, color='gray', linewidth=0.5)
        ax2.set_aspect('equal')
        ax2.grid(True, which='both', linewidth=0.5, linestyle='--')
        ax2.set_title("Output data")

        for j in range(0, self.number_of_clusters + 1):
            X_cl, Y_cl, dX_cl, dY_cl = [], [], [], []
            for i in range(0, len(self.K_list)):
                if self.K_list[i] + 1 == j:
                    dX_cl.append(data.dX[i])
                    dY_cl.append(data.dY[i])
                    X_cl.append(data.X[i])
                    Y_cl.append(data.Y[i])
            ax1.scatter(X_cl, Y_cl, marker='v', s=35, facecolors=cmap(j), edgecolors='none')
            ax2.scatter(dX_cl, dY_cl, marker='v', s=25, facecolors=cmap(j), edgecolors='none')

    def updated(self, val):

        self.number_of_clusters = int(self.noc_slider.val)
        self.angle_weight = int(self.ang_slider.val)
        self.calc_inputs_outputs_list()
        self.clustering()
        self.plot_data()
        self.fig.show()

    def inputs_button_pressed(self, val):
        self.clustering_state = "inputs"
        self.clustering()
        self.plot_data()
        self.fig.show()

    def outputs_button_pressed(self, val):
        self.clustering_state = "outputs"
        self.clustering()
        self.plot_data()
        self.fig.show()


data = Dataset()
data.read_data('dataset/kapace_training.csv', 1)
tb = ClusteringToolbox(data)
tb.calc_inputs_outputs_list()
tb.clustering()
tb.create_figure()


data = Dataset()
data.read_data('dataset/data_1_training.csv', 2)
tb = ClusteringToolbox(data)
tb.calc_inputs_outputs_list()
tb.clustering()
tb.create_figure()


data = Dataset()
data.read_data('dataset/data_2_training.csv', 2)
tb = ClusteringToolbox(data)
tb.calc_inputs_outputs_list()
tb.clustering()
tb.create_figure()
