import csv
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button


class Dataset:

    def __init__(self):
        self.X = []                         # input x coordinate
        self.Y = []                         # input y coordinate
        self.A = []                         # input angle coordinate
        self.dX = []                        # output delta x
        self.dY = []                        # output delta y
        self.dA = []                        # output delta angle
        self.X_N = []                       # input x coordinate of nearest neighbors
        self.Y_N = []                       # input y coordinate of nearest neighbors
        self.dX_N = []                      # output x coordinate of nearest neighbors
        self.dY_N = []                      # output y coordinate of nearest neighbors
        self.number_of_points = 0           # total number of data points

    def read_data(self, input_file, option=1):

        # The training and test data will be saved in these lists
        inputs, outputs = [], []

        # Read training.csv and extract the necessary data
        with open(input_file) as csvfile:
            readTrainingCSV = csv.reader(csvfile, delimiter=';')
            for row in readTrainingCSV:
                if len(row) < 9:
                    continue
                try:
                    inputs.append(np.array([float(row[2]), float(row[3]), float(row[4])]))
                    if option == 1:
                        outputs.append(np.array([float(row[6]), float(row[7]), float(row[8])]))
                    if option == 2:
                        outputs.append(np.array([float(row[5]), float(row[6]), float(row[7])]))

                except:
                    print("Row skipped")

        # Data pre-processing
        self.X, self.Y, self.A, self.dX, self.dY, self.dA = [], [], [], [], [], []

        # Write the data to vectors for each column
        for i in range(0, len(inputs)):
            self.X.append(inputs[i][0])
            self.Y.append(inputs[i][1])
            self.A.append(inputs[i][2])
            self.dX.append(outputs[i][0])
            self.dY.append(outputs[i][1])
            self.dA.append(outputs[i][2])

        self.number_of_points = len(self.X)


class DataToolbox:

    def __init__(self, data):
        # Establish new dataset object and read the data
        self.data = data
        self.selected_point = 0             # list index of currently selected point
        self.number_of_points = 0           # total number of data points
        self.number_of_neighbors = 50       # size of neighborhood
        self.angle_weight = 0               # angle weight for calculation of the neighborhood

        self.fig, (self.ax1, self.ax2) = plt.subplots(1, 2)
        self.non_slider = Slider(self.ax1, 'init', 0, 0, valinit=0, valstep=0)
        self.curr_slider = Slider(self.ax1, 'init', 0, 0, valinit=0, valstep=0)
        self.ang_slider = Slider(self.ax1, 'init', 0, 0, valinit=0, valstep=0)

    def update_neighbors(self, data):

        alpha = self.angle_weight
        k = self.selected_point

        # Calculate distances to all other points
        distances = []
        for j in range(0, len(data.X)):
            delta_x = data.X[k] - data.X[j]
            delta_y = data.Y[k] - data.Y[j]
            delta_a = data.A[k] - data.A[j]
            distances.append(np.sqrt(delta_x ** 2 + delta_y ** 2 + alpha * delta_a ** 2))

        # Find nearest neighbors
        neighbors = []
        for i in range(0, self.number_of_neighbors + 1):
            min_dist_pos = int(np.argmin(distances))
            min_dist_val = np.amin(distances)

            if min_dist_val > 0:  # ensure that the current point is excluded
                neighbors.append(min_dist_pos)  # save new neighbor
            distances[min_dist_pos] = np.infty  # 'delete' value from minimum candidates list

        # Write neighbor lists
        self.data.X_N, self.data.Y_N, self.data.dX_N, self.data.dY_N = [], [], [], []
        for i in neighbors:
            data.X_N.append(data.X[i])
            data.Y_N.append(data.Y[i])
            data.dX_N.append(data.dX[i])
            data.dY_N.append(data.dY[i])

    def plot_data(self, data):
        ax = self.ax1
        ax.cla()
        ax.scatter(data.X, data.Y, marker='o', s=8)
        ax.scatter(data.X_N, data.Y_N, marker='o', c='darkorange', s=8)
        ax.scatter(data.X[self.selected_point], data.Y[self.selected_point], marker='o', c='red', s=10)
        ax.axhline(y=0, color='gray', linewidth=0.5)
        ax.axvline(x=0, color='gray', linewidth=0.5)
        ax.set_aspect('equal')
        ax.grid(True, which='both', linewidth=0.5, linestyle='--')
        ax.set_title("Input data")

        ax = self.ax2
        ax.cla()
        ax.scatter(data.dX, data.dY, marker='o', s=8, color='red')
        ax.scatter(data.dX_N, data.dY_N, marker='o', c='black', s=8)
        ax.scatter(data.dX[self.selected_point], data.dY[self.selected_point], marker='o', c='gray', s=10)
        ax.axhline(y=0, color='gray', linewidth=0.5)
        ax.axvline(x=0, color='gray', linewidth=0.5)
        ax.set_aspect('equal')
        ax.grid(True, which='both', linewidth=0.5, linestyle='--')
        ax.set_title("Output data")

    def create_figure(self):
        self.fig.canvas.set_window_title('Neighborhood Toolbox')
        self.update_neighbors(self.data)
        self.plot_data(self.data)
        self.fig.show()

        # create interactive elements
        axcolor = 'lightblue'
        pos1 = plt.axes([0.25, 0.1, 0.65, 0.03], facecolor=axcolor)
        pos2 = plt.axes([0.25, 0.15, 0.65, 0.03], facecolor=axcolor)
        pos3 = plt.axes([0.25, 0.05, 0.35, 0.03], facecolor=axcolor)
        self.non_slider = Slider(pos2, 'Neighbors', 1, 250, valinit=50, valstep=1)
        self.curr_slider = Slider(pos1, 'Choose Pt.', 0, data.number_of_points-1, valinit=0, valstep=1)
        self.ang_slider = Slider(pos3, 'Angle weight', 0, 10, valinit=0, valstep=1)
        resetax = plt.axes([0.8, 0.025, 0.1, 0.04])
        button = Button(resetax, 'Reset', color=axcolor, hovercolor='0.975')

        self.non_slider.on_changed(self.updated)
        self.curr_slider.on_changed(self.updated)
        self.ang_slider.on_changed(self.updated)
        button.on_clicked(self.button_pressed)

        plt.subplots_adjust(left=0.1, right=0.9)
        plt.show()

    def updated(self, val):
        self.number_of_neighbors = int(self.non_slider.val)
        self.selected_point = int(self.curr_slider.val)
        self.angle_weight = self.ang_slider.val
        self.update_neighbors(self.data)
        self.plot_data(self.data)
        self.fig.show()

    def button_pressed(self, event):
        self.ax1.cla()
        self.ax2.cla()
        self.non_slider.reset()
        self.curr_slider.reset()
        self.ang_slider.reset()


data = Dataset()
data.read_data('dataset/kapace_training.csv', 1)
tb = DataToolbox(data)
tb.create_figure()


data = Dataset()
data.read_data('dataset/data_1_training.csv', 2)
tb = DataToolbox(data)
tb.create_figure()


data = Dataset()
data.read_data('dataset/data_2_training.csv', 2)
tb = DataToolbox(data)
tb.create_figure()