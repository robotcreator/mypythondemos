Toolbox:
 - BuildAToolbox.ipynb
 - ClusteringToolbox.py
 - NeighborhoodToolbox.ipynb

Analyse Datensatz 1:
 - AnalyzeDataset1.ipynb
 - AnalyzeDataset1.py

Analyse Datensatz 2:
 - AnalyzeDataset2.ipynb
 - AnalyzeDataset2.py

KNN Training (zum Vergleich):
 - IWF_Dataset_KNN_training.py


Die Jupyter Notebooks enthalten einige zusätzliche Erklärungen zum verwendeten Code,
die sich auf den Foliensatz beziehen.

Jupyter Notebook installieren:  pip install jupyterlabs
Jupyter Notebook starten: 	jupyter notebook


