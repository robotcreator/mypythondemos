import tensorflow as tf
import numpy as np
import csv
from tensorflow import keras
from tensorflow.keras import layers
import matplotlib.pyplot as plt


class GridCreator:

    def __init__(self):
        tf.compat.v1.reset_default_graph()
        self.X = 0
        self.Y = 0
        self.weights = {}
        self.biases = {}
        self.training_epochs = 1400
        self.display_step = self.training_epochs * 0.1
        self.epoch_plots = 0
        self.inputs_train = [[], [], []]
        self.outputs_train = [[], [], []]
        self.inputs_test = [[], [], []]
        self.outputs_test = [[], [], []]
        self.inputs_train_normalized = []
        self.outputs_train_normalized = []
        self.inputs_test_normalized = []
        self.outputs_test_normalized = []
        self.model = keras.Sequential([])
        self.optimizer = []
        self.errors = []

    def import_data(self, filename, option):

        with open('./dataset/' + filename + '_training.csv') as csvfile:
            training_file = csv.reader(csvfile, delimiter=';')
            for row in training_file:
                try:
                    if option == 1:
                        self.inputs_train[0].append(float(row[2]))
                        self.inputs_train[1].append(float(row[3]))
                        self.inputs_train[2].append(float(row[4]))
                        self.outputs_train[0].append(float(row[6]))
                        self.outputs_train[1].append(float(row[7]))
                        self.outputs_train[2].append(float(row[8]))
                    elif option == 2:
                        self.inputs_train[0].append(float(row[2]))
                        self.inputs_train[1].append(float(row[3]))
                        self.inputs_train[2].append(float(row[4]))
                        self.outputs_train[0].append(float(row[5]))
                        self.outputs_train[1].append(float(row[6]))
                        self.outputs_train[2].append(float(row[7]))
                except:
                    print("line passed")

            self.inputs_train = np.asarray(self.inputs_train).T
            self.outputs_train = np.asarray(self.outputs_train).T

        with open('./data/' + filename + '_test.csv') as csvfile:
            test_file = csv.reader(csvfile, delimiter=';')
            for row in test_file:
                try:
                    if option == 1:
                        self.inputs_test[0].append(float(row[2]))
                        self.inputs_test[1].append(float(row[3]))
                        self.inputs_test[2].append(float(row[4]))
                        self.outputs_test[0].append(float(row[6]))
                        self.outputs_test[1].append(float(row[7]))
                        self.outputs_test[2].append(float(row[8]))
                    elif option == 2:
                        self.inputs_test[0].append(float(row[2]))
                        self.inputs_test[1].append(float(row[3]))
                        self.inputs_test[2].append(float(row[4]))
                        self.outputs_test[0].append(float(row[5]))
                        self.outputs_test[1].append(float(row[6]))
                        self.outputs_test[2].append(float(row[7]))
                except:
                    print("line passed")

            self.inputs_test = np.asarray(self.inputs_test).T
            self.outputs_test = np.asarray(self.outputs_test).T

    def normalize_inputs(self):
        x_in = self.inputs_train.T[0] / 100
        y_in = self.inputs_train.T[1] / 100
        a_in = self.inputs_train.T[2]
        self.inputs_train = np.asarray([x_in, y_in, a_in]).T

        x_in = self.inputs_test.T[0] / 100
        y_in = self.inputs_test.T[1] / 100
        a_in = self.inputs_test.T[2]
        self.inputs_test = np.asarray([x_in, y_in, a_in]).T

    def set_optimizer(self, opt):
        if opt == 'adam':
            self.optimizer = keras.optimizers.Adam(learning_rate=0.001, beta_1=0.9, beta_2=0.999, amsgrad=False)
        elif opt == 'rms':
            self.optimizer = keras.optimizers.RMSprop(learning_rate=0.001, rho=0.9)
        elif opt == 'gdf':
            self.optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.0001)
        elif opt == 'adagrad':
            self.optimizer = keras.optimizers.Adagrad(learning_rate=0.01)
        elif opt == 'adamax':
            self.optimizer = keras.optimizers.Adamax(learning_rate=0.002, beta_1=0.9, beta_2=0.999)

    def build_model(self):
        self.model = keras.Sequential([
            layers.Dense(15, activation='sigmoid', input_shape=(3,)),
            layers.Dense(10, activation='sigmoid'),
            layers.Dense(8, activation='sigmoid'),
            layers.Dense(6, activation='sigmoid'),
            layers.Dense(3)
        ])
        self.model.compile(loss='mse',
                           optimizer=self.optimizer,
                           metrics=['mae', 'mse'])
        self.model.summary()
        return self.model

    def save_model(self, name):
        self.model.save_weights('./models/' + name + '/' + name)

    def retrain_model(self, epochs, name):
        self.model.load_weights('./models/' + name + '/' + name)

        self.model.fit(self.inputs_train, self.outputs_train,
                       epochs=epochs, batch_size=1,
                       validation_data=(self.inputs_test, self.outputs_test),
                       verbose=2)
        self.save_model(name)
        return self.model

    def train_model(self, epochs):
        self.model.fit(self.inputs_train,
                       self.outputs_train,
                       epochs=epochs, batch_size=1,
                       validation_data=(self.inputs_test, self.outputs_test),
                       verbose=2)
        return self.model

    def predict_data(self):
        y_predictions = self.model.predict(self.inputs_test)
        print(y_predictions)

    def calc_prediction_error(self, which):
        if which == 'test':
            inputs = self.inputs_test
            outputs = self.outputs_test
        elif which == 'train':
            inputs = self.inputs_train
            outputs = self.outputs_train
        outputs_pred = self.model.predict(inputs)
        x_pred = outputs_pred[:, 0]
        y_pred = outputs_pred[:, 1]
        a_pred = outputs_pred[:, 2]
        x_real = outputs[:, 0]
        y_real = outputs[:, 1]
        a_real = outputs[:, 2]
        abs_err = []
        for i in range(len(x_real)):
            abs_err.append(np.sqrt(np.square((x_pred[i] - x_real[i])) + np.square((y_pred[i] - y_real[i]))))
        print(abs_err)
        mae = np.mean(np.asarray(abs_err)).round(2)
        var = np.var(np.asarray(abs_err)).round(2)
        max_err = np.max(abs_err).round(2)
        print("Fehler:")
        print("mae = " + str(mae))
        print("var = " + str(var))
        print("Maximaler Fehler:")
        print("max_offset = " + str(max_err))
        self.errors = abs_err

    def create_model(self):
        # Create a new model instance
        self.build_model()

    def load_model(self, name):
        # Create a new model instance
        model = self.build_model()
        # Restore the weights
        model.load_weights('./models/' + name + '/' + name)

    def show_error(self, title):
        plt.plot(self.errors)
        plt.title(title)
        plt.grid(True, which='both', linewidth=0.5, linestyle='--')
        axes = plt.gca()
        axes.set_ylim([-1, 27])
        fig = plt.gcf()
        fig.set_size_inches(12, 2.5, forward=True)
        plt.show()
        return




neuralNet = GridCreator()
neuralNet.import_data('data_1', option=2)
#neuralNet.normalize_inputs()
neuralNet.set_optimizer('adamax')
neuralNet.load_model('data_1')  
neuralNet.train_model(epochs=200)
neuralNet.save_model('data_1')
neuralNet.calc_prediction_error(which='train')
neuralNet.show_error('Error on training data')
neuralNet.calc_prediction_error(which='test')
neuralNet.show_error('Error on test data')

