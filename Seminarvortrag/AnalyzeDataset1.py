import csv
import numpy as np
import matplotlib.pyplot as plt


class Dataset:

    def __init__(self):
        self.X = []                         # input x coordinate
        self.Y = []                         # input y coordinate
        self.A = []                         # input angle coordinate
        self.dX = []                        # output delta x
        self.dY = []                        # output delta y
        self.dA = []                        # output delta angle
        self.number_of_points = 0           # total number of data points

    def read_data(self, input_file, option=1):

        # The training and test data will be saved in these lists
        inputs, outputs = [], []

        # Read training.csv and extract the necessary data
        with open(input_file) as csvfile:
            readTrainingCSV = csv.reader(csvfile, delimiter=';')
            for row in readTrainingCSV:
                if len(row) < 9:
                    continue
                try:
                    inputs.append(np.array([float(row[2]), float(row[3]), float(row[4])]))
                    if option == 1:
                        outputs.append(np.array([float(row[6]), float(row[7]), float(row[8])]))
                    if option == 2:
                        outputs.append(np.array([float(row[5]), float(row[6]), float(row[7])]))

                except:
                    print("Row skipped")

        # Data pre-processing
        self.X, self.Y, self.A, self.dX, self.dY, self.dA = [], [], [], [], [], []

        # Write the data to vectors for each column
        for i in range(0, len(inputs)):
            self.X.append(inputs[i][0])
            self.Y.append(inputs[i][1])
            self.A.append(inputs[i][2])
            self.dX.append(outputs[i][0])
            self.dY.append(outputs[i][1])
            self.dA.append(outputs[i][2])

        self.number_of_points = len(self.X)


# Establish new dataset object and read the data
data = Dataset
data.read_data(data, 'dataset/kapace_training.csv', 1)
print("\nX data:")
print(data.X[0:10])
print("\nY data:")
print(data.Y[0:10])
print("\nA data:")
print(data.A[0:10])


def calc_geometric_angles(dataset):
    geometric_angles = []
    input_angles = np.asarray(dataset.A)

    # Calculate output geometric angles of all points
    for i in range(0, dataset.number_of_points):
        xi = dataset.dX[i]
        yi = dataset.dY[i]
        phi = -90 - np.arctan2(yi, xi) * (180 / np.pi)
        phi = np.round(phi, 1)
        geometric_angles.append(phi)

    return geometric_angles


g = calc_geometric_angles(data)
print(g[0:10])


def plot_geometric_angles(input_angles, geometric_angles):

    fig, ax = plt.subplots()
    ax.scatter(input_angles, geometric_angles, s=3)
    ax.grid(True, which='both', linewidth=0.5, linestyle='--')
    ax.axhline(y=0, color='gray', linewidth=0.5)
    ax.axvline(x=0, color='gray', linewidth=0.5)
    ax.set_ylabel('output angle')
    ax.set_xlabel('input angle')
    ax.set_title('Angle relations (first estimation)')
    plt.show()

plot_geometric_angles(data.A, g)


def fit_polynomial_to_data(x_data, y_data, degree):

    coeffs, error, _, _, _ = np.polyfit(x_data, y_data, degree, full=True)
    function = np.poly1d(coeffs)
    x_axis = np.linspace(np.asarray(x_data).min(), np.asarray(x_data).max(), 100)
    print("Koeffizienten: " + str(coeffs))
    print("Relativer Fehler: " + str(error/len(x_data)))

    return function, x_axis

p, xp = fit_polynomial_to_data(data.A, g, 1)


def plot_linear_regression(input_angles, geometric_angles, f, x_axis):
    fig, ax = plt.subplots()
    ax.scatter(input_angles, geometric_angles, s=3)
    ax.plot(x_axis, f(x_axis), '-', color='red', linewidth=2)
    ax.grid(True, which='both', linewidth=0.5, linestyle='--')
    ax.axhline(y=0, color='gray', linewidth=0.5)
    ax.axvline(x=0, color='gray', linewidth=0.5)
    ax.set_ylabel('output angle')
    ax.set_xlabel('input angle')
    ax.set_title('Angle relations (first estimation)')
    plt.show()


plot_linear_regression(data.A, g, p, xp)


def reject_outliers(input_angles, geometric_angles, f, distance_threshold):
    reject_list = []
    for i in range(0, len(input_angles)):
        if np.abs(geometric_angles[i] - f(input_angles[i])) > distance_threshold:
            reject_list.append(i)
    filtered_geometric = np.delete(geometric_angles, reject_list)
    filtered_inputs = np.delete(input_angles, reject_list)
    return filtered_inputs, filtered_geometric


f_inputs, f_geometric = reject_outliers(data.A, g, p, 50)
p, xp = fit_polynomial_to_data(f_inputs, f_geometric, 1)
plot_linear_regression(data.A, g, p, xp)



import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def calc_radius(dataset):

    radius_list = []

    for i in range(0, dataset.number_of_points):
        radius_list.append(np.sqrt(dataset.dX[i] ** 2 + dataset.dY[i] ** 2))

    return radius_list


def plot_points_3D(dataset):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(dataset.X, dataset.Y, calc_radius(dataset))
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('Output radius')
    plt.show()

plot_points_3D(data)


def fit_3D_plane(dataset):
    r = np.asarray(calc_radius(dataset))
    XY = np.asarray([dataset.X, dataset.Y]).T  # transpose so input vectors are along the rows
    factors = np.c_[XY, np.ones(XY.shape[0])]  # add bias term
    coeffs = np.linalg.lstsq(factors, r, rcond=None)[0]
    return coeffs

plane_coefficients = fit_3D_plane(data)


def plot_points_and_plane(dataset, coeffs):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(dataset.X, data.Y, calc_radius(dataset))
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('Output radius')

    xx, yy = np.meshgrid(np.arange(-150, 151), np.arange(-150, 151))
    z = (coeffs[0] * xx + coeffs[1] * yy) + coeffs[2]
    ax.plot_surface(xx, yy, z, alpha=0.2)
    plt.show()

plot_points_and_plane(data, plane_coefficients)
print(fit_3D_plane(data))


# Rotation investigation

# plt.scatter(data.A, data.dA, marker='.')
# plt.show()

# plt.scatter(g, data.dA, marker='.')
# plt.show()

# plt.scatter(data.X, data.dA, marker='.')
# plt.show()

# plt.scatter(data.Y, data.dA, marker='.')
# plt.show()


# Performance Analyse

def calc_model_error(dataset):
    deg2rad = np.pi / 180
    w0 = 1.79543597e+01
    w1 = 1.34632446e-02
    w2 = 1.88545630e-02

    abs_err = []
    for i in range(0, dataset.number_of_points):
        delta_est = 1.05070577 * dataset.A[i] + 10.01902907
        r_est = w0 + dataset.X[i] * w1 + dataset.Y[i] * w2
        x_est = - r_est * np.sin(delta_est * deg2rad)
        y_est = - r_est * np.cos(delta_est * deg2rad)
        abs_err.append(np.sqrt(np.square(x_est - dataset.dX[i]) + np.square((y_est - dataset.dY[i]))))

    return abs_err


def plot_position_error(dataset, title):
    abs_err = calc_model_error(dataset)

    plt.plot(abs_err)
    plt.title(title)
    plt.grid(True, which='both', linewidth=0.5, linestyle='--')
    axes = plt.gca()
    axes.set_ylim([-1, 27])
    fig = plt.gcf()
    fig.set_size_inches(12, 2.5, forward=True)
    plt.show()

train_data = Dataset()
train_data.read_data('dataset/kapace_training.csv', 1)
test_data = Dataset()
test_data.read_data('dataset/kapace_test.csv', 1)

plot_position_error(train_data, 'Error on training data')
plot_position_error(test_data,'Error on test data')


def calc_error_measures(dataset):
    abs_err = calc_model_error(dataset)

    mae = np.mean(np.asarray(abs_err)).round(2)
    var = np.var(np.asarray(abs_err)).round(2)
    max_err = np.max(abs_err).round(2)
    return [mae, max_err, var]


[mae, max_err, var] = calc_error_measures(train_data)
print("Fehler Trainingsdaten:")
print("mae = " + str(mae))
print("max = " + str(max_err))
print("var = " + str(var))

[mae, max_err, var] = calc_error_measures(test_data)
print("Fehler Testdaten:")
print("mae = " + str(mae))
print("max = " + str(max_err))
print("var = " + str(var))