import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button
from Kinematics import Kinematics


# Define link lengths for the 2R robot

L1 = 430
L2 = 430
robot = Kinematics(L1, L2)

cmode = ['fk']              # valid inputs: 'fk' or 'ik'

# create plot
fig, ax = plt.subplots()
fig.canvas.set_window_title('2R Robot Simulator')

# control elements
x_slider_ax = plt.axes([0.1, 0.1, 0.30, 0.03], facecolor='lightblue')
x_slider = Slider(x_slider_ax, 'x', -800, 800, valinit=0, valstep=1)
y_slider_ax = plt.axes([0.1, 0.05, 0.30, 0.03], facecolor='lightblue')
y_slider = Slider(y_slider_ax, 'y', -800, 800, valinit=0, valstep=1)

q1_slider_ax = plt.axes([0.6, 0.1, 0.30, 0.03], facecolor='lightblue')
q1_slider = Slider(q1_slider_ax, 'q1', -140, 140, valinit=0, valstep=1)
q2_slider_ax = plt.axes([0.6, 0.05, 0.30, 0.03], facecolor='lightblue')
q2_slider = Slider(q2_slider_ax, 'q2', -155, 155, valinit=0, valstep=0.1)

buttonax = plt.axes([0.45, 0.01, 0.1, 0.04])
button = Button(buttonax, 'Update', color='lightblue', hovercolor='0.975')


def plot_robot(ax, x, y, points):
    ax.cla()
    ax.set_xlim([-1300, 1300])
    ax.set_ylim([-900, 900])
    ax.axhline(y=0, color='gray', linewidth=0.5)
    ax.axvline(x=0, color='gray', linewidth=0.5)
    ax.grid(True, which='both', linewidth=0.5, linestyle='--')
    ax.set_aspect('equal')
    [P0, P1, P2] = points
    ax.plot([P0[0], P1[0]], [P0[1], P1[1]], 'k-')
    ax.plot([P1[0], P2[0]], [P1[1], P2[1]], 'k-')
    ax.plot(P0[0], P0[1], "o", markeredgecolor='black', markersize = 10)
    ax.plot(P1[0], P1[1], "o", markeredgecolor='black', markersize = 10)
    ax.plot(x, y, "o", markeredgecolor='black', markersize = 10)
    fig.show()


def check_borders(q1, q2):
    return np.abs(q1) <= 140 and np.abs(q2) <= 155


def updated_fk(val):
    q1 = q1_slider.val
    q2 = q2_slider.val
    robot.fkin(q1, q2)
    [x1, x2] = robot.tcp
    plot_robot(ax, x1, x2, robot.points)
    print("x: " + str(np.round(x2, 2)) + "mm | y: " + str(np.round(x2, 2))
          + "mm | q1: " + str(np.round(q1, 2)) + "° | q2: " + str(np.round(q2, 2)) + "°  " + str(cmode))
    cmode[0] = 'fk'


def updated_ik(val):
    x1 = x_slider.val
    x2 = y_slider.val
    robot.ikin(x1, x2)
    [q1, q2] = robot.joints
    if not check_borders(q1, q2):
        print("BORDERS REACHED")
    robot.fkin(q1, q2)
    plot_robot(ax, robot.tcp[0], robot.tcp[1], robot.points)
    print("x: " + str(np.round(x1, 2)) + "mm | y: " + str(np.round(x2, 2))
          + "mm | q1: " + str(np.round(q1, 2)) + "° | q2: " + str(np.round(q2, 2)) + "°  " + str(cmode))
    cmode[0] = 'ik'


def button_pressed(event):
    if cmode[0] == 'ik':
        x1 = x_slider.val
        x2 = y_slider.val
        [q1, q2] = ikin(x1, x2)
        q1_slider.set_val(q1)
        q2_slider.set_val(q2)

    elif cmode[0] == 'fk':
        q1 = q1_slider.val
        q2 = q2_slider.val
        robot.fkin(q1, q2)
        [x1, x2] = robot.tcp
        x_slider.set_val(x1)
        y_slider.set_val(x2)
  


x_slider.on_changed(updated_ik)
y_slider.on_changed(updated_ik)
q1_slider.on_changed(updated_fk)
q2_slider.on_changed(updated_fk)
button.on_clicked(button_pressed)


plt.subplots_adjust(left=0.1, right=0.9, bottom=0.2)
plt.show()