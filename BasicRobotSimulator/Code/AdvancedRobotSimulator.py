import numpy as np
import matplotlib.pyplot as plt
import tinyik

arm = tinyik.Actuator(['z', [1, 0., 0.], 'z', [1., 0., 0.], 'z', [1., 0., 0.]])
print(arm.angles)
print(arm.ee)
tinyik.visualize(arm)