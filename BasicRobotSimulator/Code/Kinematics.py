import numpy as np

# source: Alshamasin et al, "Kinematic Modeling and Simulation of a SCARA
# Robot by Using Solid Dynamics and Verification by
# MATLAB/Simulink"

class Kinematics:

    def __init__(self, L1, L2):
        
        self.links = [L1, L2]
        self.joints = [0, 0]
        self.tcp = [0, 0]
        self.points = []
    
    def r2d(self, radians):
        return (np.asarray(radians)*(180/np.pi)).tolist()
    
    def d2r(self, degrees):
        return (np.asarray(degrees)*(np.pi/180)).tolist()
    
    def fkin(self, q1, q2):
        [L1, L2] = self.links
        q1 = self.d2r(q1)
        q2 = self.d2r(q2)
        
        self.tcp[0] = L1*np.cos(q1) + L2*np.cos((q1+q2))
        self.tcp[1] = L1*np.sin(q1) + L2*np.sin((q1+q2))

        P0 = [0, 0]
        P1 = [L1*np.cos(q1), L1*np.sin(q1)]
        P2 = self.tcp
        self.points = [P0, P1, P2]
        
        return True


    def ikin(self, x1, x2):
        [L1, L2] = self.links
        
        L12 = np.sqrt(x1**2 + x2**2)
        a = (L1**2 + L12**2 - L2**2) / (2*L1*L12)
        # hier muss eine von zwei Lösungen selektiert werden
        q1 = np.arctan2(x2, x1) - np.arctan2(np.sqrt(1-a**2), a)
        q2 = np.arctan2(x2 - L1*np.sin(q1), x1 - L1*np.cos(q1)) - q1
        self.joints = [np.round(self.r2d(q1), 2), np.round(self.r2d(q2), 2)]
        
        return True
