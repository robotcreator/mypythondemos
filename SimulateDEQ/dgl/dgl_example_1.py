import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp

sol = solve_ivp(lambda t, y: t-y, [0, 15], [10], t_eval=np.linspace(0, 15, 100))

print("Simple example")
print(sol.t)
print(sol.y)

plt.plot(sol.t, sol.y[0], 'k--s')
plt.show()
