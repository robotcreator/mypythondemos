# %% Imports
import numpy as np
from scipy.integrate import solve_ivp
from dgl.ode_helpers import state_plotter

# %% Define independent function and derivative function
def f(t, y, c):
    dydt = [c[0]]
    return dydt

# %% Define time spans, initial values, and constants
tspan = np.linspace(0, 10, 100)
yinit = [6]
c = [1.2]

# %% Solve differential equation
sol = solve_ivp(lambda t, y: f(t, y, c),
                [tspan[0], tspan[-1]], yinit, t_eval=tspan)

# %% Plot states
state_plotter(sol.t, sol.y, 1)


# %% Define derivative function
def f(t, y, c):
    dydt = np.polyval(c, t)
    return dydt

# %% Define time spans, initial values, and constants
tspan = np.linspace(0, 4, 20)
yinit = [6]
c = [2, -6, 3]

# %% Solve differential equation
sol = solve_ivp(lambda t, y: f(t, y, c),
                [tspan[0], tspan[-1]], yinit, t_eval=tspan)

# %% Plot states
state_plotter(sol.t, sol.y, 1)



# %% Define derivative function

def f(t, y, c):
    dydt = [c[0] * y[0]]
    return dydt

# %% Define time spans, initial values, and constants
tspan = np.linspace(0, 3, 25)
yinit = [10]
c = [1.02]

# %% Solve differential equation
sol = solve_ivp(lambda t, y: f(t, y, c),
                [tspan[0], tspan[-1]], yinit, t_eval=tspan)

# %% Plot states
state_plotter(sol.t, sol.y, 1)
