# -*- coding: utf-8 -*-
"""
Totzeit mit P-T1 Pade-Approximation
Created on 10.4. 2019
@author: philippsen
"""
import control
import control.matlab
import numpy as np
from matplotlib.pyplot import *
import matplotlib.pyplot as plt
T = 1       # Zeitkonstante P-T1
TT = 0.5    # Totzeit
n = 500 # Anzahl Schritte
T0 = 0.01 # Abtastzeit
u = np.zeros(n)     # Vektor Eingangsgröße
tk= np.arange(0,T0*n,T0)    # Zeit- bzw. k-Achse
ab = round(TT/T0)
for i in range(ab, n):  # Verschiebung um Totzeit
     u[i] = 1.0
G1 = control.tf([ 1],[T, 1]) # P-T1
yout, tk, xout = control.matlab.lsim(G1, u, tk, X0=0)
num, den = control.matlab.pade(0.5,3)   # Pade Appr. 3.Ordnung
Gpade = control.matlab.tf(num,den)
x, tk = control.matlab.step(G1*Gpade, tk, X0=0)
plt.plot(tk,yout, tk, x)
plt.title('Totzeit-P-T1 und Pade-Approx.')
plt.xlabel('t [s]'); plt.ylabel('x')
plt.grid(b=True)
plt.figure()
w, mag, phase = control.bode(G1*Gpade)
plt.show()