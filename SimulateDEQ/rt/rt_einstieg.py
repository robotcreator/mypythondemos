# -*- coding: utf-8 -*-
"""
P-Tn  Reihenschaltung 3 P-T1
Created on 15.4. 2019
@author: philippsen
"""
import numpy as np
import control
from   control.matlab import *
import matplotlib.pyplot as plt
Kp= 3.0    #    Verstärkung
T1 = 2.0   #    Zeitkonstanten
T2 = 3.0
T3 = 5.0
N1  = np.array([T1, 1])
N2  = np.array([T2, 1])
N3  = np.array([T3, 1])
G1 = tf(Kp,N1)
G2 = tf(1.0,N2)
G3 = tf(1.0,N3)
Gges = G1*G2*G3
x3, t = control.matlab.step(Gges)
x2, t = control.matlab.step(G1*G2,t)
x1, t = control.matlab.step(G1,t)
plt.plot(t, x1, t, x2,t, x3, "b")
plt.ylabel('x1, x2, x3', color="blue", fontsize=14)
plt.xlabel('t [s]', fontsize=12)
plt.title('Reihenschaltung drei P-T1', fontsize=12)
plt.grid()
plt.figure()
real, imag, ww = control.matlab.nyquist(Gges,Plot=False)
plt.plot(real,imag)
plt.title('Ortskurve P-T3')
plt.xlabel('Real'); plt.ylabel('Imag')
plt.grid()
plt.show()