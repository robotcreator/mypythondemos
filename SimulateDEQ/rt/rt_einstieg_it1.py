# -*- coding: utf-8 -*-
"""
P-T1 in Reihenschaltung mit I-Glied
Created on 15.4. 2019
@author: philippsen
"""
import numpy as np
import control
from   control.matlab import *
import matplotlib.pyplot as plt
Kp= 2.0    #    Verstärkung
T1 = 0.2   #    Zeitkonstante
N1  = np.array([T1, 1])
G1 = tf(Kp,N1)  # P-T1
Gi = tf(1.0,[1.0, 0])  # Integrator
G4 = G1*Gi          # I-T1
time = np.arange(0.0, 1.0, 0.001)
x4, tt = control.matlab.step(G4,time)
plt.figure()
plt.plot(tt, x4, "r")
plt.ylabel('x4', color="red", fontsize=14)
plt.xlabel('t [s]', fontsize=12)
plt.title('I-T1', fontsize=12)
plt.grid()
plt.figure()
bode(G4)
plt.figure()
real, imag, ww = control.matlab.nyquist(G4, labelFreq=10)
plt.plot(real,imag)
plt.title('Ortskurve I-T1')
plt.xlabel('Real'); plt.ylabel('Imag')
plt.grid()
plt.show()