# -*- coding: utf-8 -*-
"""
DC-Motor  G(s)  Faulhaber Motor 2237
Created on 2.4. 2019
@author: philippsen
"""
import numpy as np; from math import pi
import control
from   control.matlab import *
import matplotlib.pyplot as plt
cPsi= 0.0318    #   Datenblatt Faulhaber 2237
R = 15.7        # Widerstand Anker
L = 590e-6        # Induktivität Anker
zpiJ = 47.119e-6 #  Schwungmasse Berechnung 2pi7,5e-6
#zpiJ = 2*pi*75e-7
Kr = 35.82e-6
zN  = np.array([cPsi/R ])
nN  = np.array([ zpiJ, Kr+(2*pi*cPsi**2)/R])
zI  = np.array([zpiJ/R , Kr/R])
DCmot = tf(zN,nN)
n, t = control.matlab.step(DCmot)
DCmotI = tf(zI,nN)
ii, tt = control.matlab.step(DCmotI)
fig, ax1 = plt.subplots()
ax1.plot(tt, n*60*5, "b")
ax1.set_ylabel('n [1/min]', color="blue", fontsize=14)
plt.grid()
ax2 = ax1.twinx()
ax2.plot(tt, ii*5*1000, "r")
ax2.set_ylabel('i [mA]', color="red", fontsize=14)
ax1.set_xlabel('t [s]', fontsize=12)
ax1.set_title('DC-Motor 2237', fontsize=12)
plt.show()
