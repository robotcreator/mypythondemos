
add_ten = lambda a : a + 10
print(add_ten(0))

multiply = lambda a, b : a * b
print(multiply(5, 6))

add_three = lambda a, b, c : a + b + c
print(add_three(0, 0, 0))


def multiplier(n):
  return lambda a : a * n

mytripler = multiplier(3)
print(mytripler(11))
