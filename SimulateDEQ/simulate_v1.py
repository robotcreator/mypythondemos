from scipy.integrate import odeint
import matplotlib.pyplot as plt
import numpy as np


class DynamicalSystem:

    def __init__(self):
        self.m = 1.5
        self.k = -2.5
        self.g = 9.8
        self.t = []
        self.state0 = []
        self.state = []

    def set_time_axis(self, total_time, delta_t):
        self.t = np.arange(0.0, total_time, delta_t)
        print(self.t)

    def init_system_state(self):
        state0 = [0.0, 0.0]

    def mass_spring(self, state_vector, t):
        x = state_vector[0]
        xd = state_vector[1]
        xdd = ((self.k*x)/self.m) + self.g          # compute acceleration xdd
        return [xd, xdd]                            # return the two state derivatives

    def integrate(self):
        self.state = odeint(self.mass_spring, self.state0, self.t)
        print(self.state)

    def plot(self):
        plt.plot(self.t, self.state)
        plt.xlabel('TIME (sec)')
        plt.ylabel('STATES')
        plt.title('Mass-Spring System')
        plt.legend(('$x$ (m)', '$\dot{x}$ (m/sec)'))
        plt.show()


ds = DynamicalSystem()
ds.set_time_axis(10, 0.1)
ds.init_system_state()
#ds.integrate()
#ds.plot()


