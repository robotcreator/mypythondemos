from tensorflow.keras.datasets import mnist
import tensorflow.keras.backend as K
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Flatten
from tensorflow.keras.losses import categorical_crossentropy
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.layers import Dropout
import matplotlib.pyplot as plt
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import MaxPooling2D

train_da, test_da = mnist.load_data()
x_train, y_train = train_da
x_test, y_test = test_da

data_format = K.image_data_format()
rows, cols = 28, 28
num_classes = 10
train_size = x_train.shape[0]
test_size = x_test.shape[0]

if data_format == "channels_first":
    x_train = x_train.reshape(train_size, 1, rows, cols)
    x_test = x_test.reshape(test_size, 1, rows, cols)
    input_shape = (1, rows, cols)
else:
    x_train = x_train.reshape(train_size, rows, cols, 1)
    x_test = x_test.reshape(test_size, rows, cols, 1)
    input_shape = (rows, cols, 1)

# norm data to float in range 0 ... 1
x_train = x_train.astype('float32')
x_test = x_test.astype('float32')

# divide pixel values by 255 (max value)

x_train /= 255
x_test /= 255

# conv class vecs to one hot vec
y_train = to_categorical(y_train, 10)
y_test = to_categorical(y_test, 10)

x_train = x_train[:1000]
y_train = y_train[:1000]


model = Sequential()
model.add(Conv2D(32, kernel_size=(3, 3), activation='relu', input_shape=input_shape))
model.add(Conv2D(64, kernel_size=(3, 3), activation='relu'))
model.add(MaxPooling2D(
             pool_size=(2, 2)))
model.add(Dropout(rate=0.25))
model.add(Flatten(input_shape=input_shape))
model.add(Dense(200, activation='relu'))
model.add(Dropout(rate=0.5))
model.add(Dense(num_classes, activation='softmax'))
print(model.summary())


epochs = 12
batch_size=128

model.compile(loss=categorical_crossentropy, optimizer=Adam(), metrics=['accuracy'])
history = model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs, verbose=1,
                    validation_data=(x_test, y_test))

score = model.evaluate(x_test, y_test, verbose=0)
print('Test loss:', score[0])
print('Test accuracy:', score[1])



fig1, ax = plt.subplots()
ax.plot(history.epoch, history.history['loss'])            # Loss auf Trainingsdaten
ax.plot(history.epoch, history.history['val_loss'])       # Loss auf Testdaten
ax.set_title('Loss')
ax.set_ylabel('Loss')
ax.set_xlabel('Epoche')
ax.legend(['Trainingsdaten', 'Testdaten'])

fig2, ax = plt.subplots()
ax.plot(history.epoch, history.history['accuracy'])        # Accuracy auf Trainingsdaten
ax.plot(history.epoch, history.history['val_accuracy'])   # Accuracy auf Testdaten
ax.set_title('Accuracy')
ax.set_ylabel('Accuracy')
ax.set_xlabel('Epoche')
ax.legend(['Trainingsdaten', 'Testdaten'])

fig1.show()
fig2.show()

